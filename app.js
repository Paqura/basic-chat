const express = require('express');
const app = express();
var http = require('http').Server(app);
const path = require('path');
var io = require('socket.io')(http);

const port = 8080;

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, './index.html'));
});

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('send message', (msg) => {
    io.emit('send message', msg);
  });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(port, function(){
  console.log('listening on *:8080');
});
    